package com.lj.employeeapi.repository;

import com.lj.employeeapi.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    Employee getEmployeesById(Long id);

    @Query(value = "select e.* from employee e left join supervisor s on e.supervisor_s_id=s.s_id where s.s_id=:id",nativeQuery = true)
    List<Employee> findEmployees(@Param("id") Long id);


//    @Query(value = "SELECT u FROM User u WHERE u.name IN :names")
//    List<User> findUserByNameList(@Param("names") Collection<String> names);
}
