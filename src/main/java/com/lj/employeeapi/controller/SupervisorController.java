package com.lj.employeeapi.controller;

import com.lj.employeeapi.domain.Employee;
import com.lj.employeeapi.domain.Supervisor;
import com.lj.employeeapi.service.EmployeeService;
import com.lj.employeeapi.service.SupervisorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("supervisor")
@CrossOrigin(origins = "*")
public class SupervisorController {

    private SupervisorService supervisorService;
    private EmployeeService employeeService;

    @Autowired
    public SupervisorController(SupervisorService supervisorService, EmployeeService employeeService) {
        this.supervisorService = supervisorService;
        this.employeeService = employeeService;
    }
    @GetMapping
    public List<Supervisor> viewAllSupervisors(){
        return supervisorService.viewAllSupervisors();
    }
    @PostMapping
    public Supervisor addSupervisor(@RequestBody Supervisor supervisor){
        return supervisorService.addSupervisor(supervisor);
    }
    @PutMapping
    public Supervisor editSupervisor(@RequestParam Long id,@RequestBody Supervisor supervisor){
        return supervisorService.editSupervisor(id,supervisor);
    }
    @DeleteMapping
    public void deleteSupervisor(@RequestParam Long id){
        supervisorService.deleteSupervisor(id);
    }
    @GetMapping("getId")
    public Optional getSupervisorById(@RequestParam Long id) {
        return supervisorService.getSupervisorById(id);

    }
}
