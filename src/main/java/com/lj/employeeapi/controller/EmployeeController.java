package com.lj.employeeapi.controller;

import com.lj.employeeapi.domain.Employee;
import com.lj.employeeapi.service.EmployeeService;
import com.lj.employeeapi.service.SupervisorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("employee")
@CrossOrigin(origins = "*")
public class EmployeeController {

    private EmployeeService employeeService;
    private SupervisorService supervisorService;

    @Autowired
    public EmployeeController(EmployeeService employeeService, SupervisorService supervisorService) {
        this.employeeService = employeeService;
        this.supervisorService = supervisorService;
    }

    @GetMapping
    public List<Employee> viewAllEmployees(){
        return employeeService.viewAllEmployees();
    }


    @PostMapping
    public Employee addEmployee(@RequestBody Employee employee){
        return employeeService.addEmployee(employee);
    }

    @PutMapping
    public Employee editEmployee(@RequestParam Long id,@RequestBody Employee employee){
        return employeeService.editEmployee(id,employee);
    }
    @DeleteMapping
    public void deleteStudent(@RequestParam Long id){
        employeeService.deleteEmployee(id);
    }
    @GetMapping("getId")
    public Employee getEmployeeById(@RequestParam Long id){
       return employeeService.getEmployeeById(id);
    }

    @GetMapping("sId")
    public List <Employee> getEmployeesBySupervisorSId(@RequestParam Long id){
        return employeeService.getEmployeesBySupervisorSId(id);
    }
}
