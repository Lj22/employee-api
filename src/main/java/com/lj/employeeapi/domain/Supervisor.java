package com.lj.employeeapi.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class Supervisor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long sId;
    private String sLastName;
    private String sFirstName;
    private String sGender;

    @OneToMany(mappedBy = "supervisor")
    private List<Employee> employeeList;

    public Supervisor() {
    }

    public Supervisor(Long sId, String sLastName, String sFirstName, String sGender, List<Employee> employeeList) {
        this.sId = sId;
        this.sLastName = sLastName;
        this.sFirstName = sFirstName;
        this.sGender = sGender;
        this.employeeList = employeeList;
    }

    public Long getsId() {
        return sId;
    }

    public void setsId(Long sId) {
        this.sId = sId;
    }

    public String getsLastName() {
        return sLastName;
    }

    public void setsLastName(String sLastName) {
        this.sLastName = sLastName;
    }

    public String getsFirstName() {
        return sFirstName;
    }

    public void setsFirstName(String sFirstName) {
        this.sFirstName = sFirstName;
    }

    public String getsGender() {
        return sGender;
    }

    public void setsGender(String sGender) {
        this.sGender = sGender;
    }



    @Override
    public String toString() {
        return "Supervisor{" +
                "sId=" + sId +
                ", sLastName='" + sLastName + '\'' +
                ", sFirstName='" + sFirstName + '\'' +
                ", sGender='" + sGender + '\'' +
                ", employeeList=" + employeeList +
                '}';
    }
}
