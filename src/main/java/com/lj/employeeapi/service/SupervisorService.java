package com.lj.employeeapi.service;

import com.lj.employeeapi.domain.Employee;
import com.lj.employeeapi.domain.Supervisor;
import com.lj.employeeapi.repository.EmployeeRepository;
import com.lj.employeeapi.repository.SupervisorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SupervisorService {
    private SupervisorRepository supervisorRepository;
    private EmployeeRepository employeeRepository;

    @Autowired
    public SupervisorService(SupervisorRepository supervisorRepository, EmployeeRepository employeeRepository) {
        this.supervisorRepository = supervisorRepository;
        this.employeeRepository = employeeRepository;
    }


    //View
    public List<Supervisor> viewAllSupervisors() {
        return supervisorRepository.findAll();
    }

    //Add
    public Supervisor addSupervisor(Supervisor supervisor) {
        return supervisorRepository.save(supervisor);
    }

    public Supervisor editSupervisor(Long id, Supervisor supervisor) {
        List<Supervisor> supervisorList = viewAllSupervisors();
        supervisor.setsId(id);
        return supervisorRepository.save(supervisor);
    }

    public void deleteSupervisor(Long id) {
        List<Supervisor> supervisorList = viewAllSupervisors();
        supervisorRepository.deleteById(id);
    }

    public Optional<Supervisor> getSupervisorById(Long id) {
        return supervisorRepository.findById(id);
    }

}
