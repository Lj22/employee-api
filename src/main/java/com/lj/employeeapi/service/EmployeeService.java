package com.lj.employeeapi.service;
import com.lj.employeeapi.domain.Employee;
import com.lj.employeeapi.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    private EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> viewAllEmployees() {
        return employeeRepository.findAll();

    }

    public Employee addEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    public Employee editEmployee(Long id, Employee employee) {
        List<Employee> employeeList = viewAllEmployees();
        employee.setId(id);
        return employeeRepository.save(employee);
    }

    public void deleteEmployee(Long id) {
        List<Employee> employeeList = viewAllEmployees();
        employeeRepository.deleteById(id);
    }

    public Employee getEmployeeById(Long id) {
       return employeeRepository.getEmployeesById(id);
    }
    public List<Employee> getEmployeesBySupervisorSId(Long id){
        return employeeRepository.findEmployees(id);
    }
}



